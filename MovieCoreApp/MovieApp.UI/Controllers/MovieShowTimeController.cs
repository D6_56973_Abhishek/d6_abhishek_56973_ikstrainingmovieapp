﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MovieApp.Entity;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MovieApp.UI.Controllers
{
    public class MovieShowTimeController : Controller
    {

        IConfiguration _configuration;
        public MovieShowTimeController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public async Task<IActionResult> ShowMovieTime()
        {
            ViewBag.status = "";
            using (HttpClient client = new HttpClient())
            {
               
                string endPoint = _configuration["WebAPiURL"] + "MovieShowTime/ShowMovieShowTimeList";
                using (var response = await client.GetAsync(endPoint))
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var result = await response.Content.ReadAsStringAsync();
                        var movieShowTimeModel = JsonConvert.DeserializeObject<List<MovieShowTimeModel>>(result);

                        return View(movieShowTimeModel);
                       
                    }

                    else
                    {
                        ViewBag.status = "Error";
                        ViewBag.message = "No entries found!";
                    }
                }
            }

            return View();
        }

    
        [HttpPost]
        public async Task<IActionResult> InsertMovieShowTimes([Bind("ShowId,MovieId,TheatreId,ShowTime,Date")] MovieShowTimeModel movieShowTimeModel)
        {

            ViewBag.status = "";
            using (HttpClient client = new HttpClient())
            {
                StringContent content = new StringContent(JsonConvert.SerializeObject(movieShowTimeModel), Encoding.UTF8, "application/json");
                string endPoint = _configuration["WebAPiURL"] + "MovieShowTime/InsertMovieShowTime";
                using (var response = await client.PostAsync(endPoint, content))
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        ViewBag.status = "Ok";
                        ViewBag.message = "Inserted successfully!";
                    }
                    else
                    {
                        ViewBag.status = "Error";
                        ViewBag.message = "Wrong entries!";
                    }
                }
            }
            return View();
        }
    }
    }

