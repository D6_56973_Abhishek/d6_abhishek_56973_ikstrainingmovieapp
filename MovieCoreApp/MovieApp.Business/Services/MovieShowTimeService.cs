﻿using MovieApp.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace MovieApp.Business.Services
{
    public class MovieShowTimeService
    {
        IMovieShowTime _movieShowTime; 
        public MovieShowTimeService(IMovieShowTime movieShowTime)

        { 
            _movieShowTime = movieShowTime;

        }

        public string InsertMovieShowTime(MovieApp.Entity.MovieShowTimeModel movieShowTimeModel)
        {
            return _movieShowTime.InsertMovieShowTime(movieShowTimeModel);
        }
        public List<MovieApp.Entity.MovieShowTimeModel> ShowMovieShowTimeList()
        {
            return _movieShowTime.ShowMovieShowTime();
        }

    }
}
