﻿using Microsoft.AspNetCore.Mvc;
using MovieApp.Business.Services;
using MovieApp.Entity;

namespace MovieAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieShowTimeController: ControllerBase
    {
        MovieShowTimeService _MovieShowTimeService;

        public MovieShowTimeController(MovieShowTimeService movieShowTimeService)
        {
            _MovieShowTimeService = movieShowTimeService;
        }

        [HttpGet("ShowMovieShowTimeList")]
        public IActionResult ShowMovieShowTimeList()
        {
            return Ok(_MovieShowTimeService.ShowMovieShowTimeList());
        }

        [HttpPost("InsertMovieShowTime")]
        public IActionResult InsertMovieShowTime(MovieShowTimeModel movieShowTimeModel)
        {
            return Ok(_MovieShowTimeService.InsertMovieShowTime(movieShowTimeModel));
        }
    }
}
