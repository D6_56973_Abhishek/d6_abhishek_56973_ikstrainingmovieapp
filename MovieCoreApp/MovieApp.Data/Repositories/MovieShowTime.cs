﻿using MovieApp.Data.DataConnection;
using MovieApp.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MovieApp.Data.Repositories
{
    public class MovieShowTime : IMovieShowTime
    {
        MovieDbContext _movieDbContext;


        public MovieShowTime(MovieDbContext movieDbContext)
        {
            _movieDbContext = movieDbContext;

        }
        public string InsertMovieShowTime(MovieShowTimeModel movieShowTimeModel)
        {
            _movieDbContext.movieShowTimeModel.Add(movieShowTimeModel);
            _movieDbContext.SaveChanges();
            return "Inserted Movie Time";
        }


        public List<MovieShowTimeModel> ShowMovieShowTime()
        {
            return _movieDbContext.movieShowTimeModel.ToList();
        }
    }
}

