﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieApp.Data.Repositories
{
    public interface IMovieShowTime
    {
        string InsertMovieShowTime(MovieApp.Entity.MovieShowTimeModel movieShowTimeModel);
        List<MovieApp.Entity.MovieShowTimeModel> ShowMovieShowTime();
    }
}
